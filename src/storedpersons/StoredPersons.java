package storedpersons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StoredPersons {

	public static void main(String[] args) {

		// get list of Persons
		List<Person> persons = getPersons();

		Map<String, List<Person>> map = new HashMap<>();

		for (Person p : persons) {

			// if the Map already contains the given first name
			if (map.containsKey(p.getFirstName())) {
				// add Person to List
				map.get(p.getFirstName()).add(p);
			} else {
				// if the Map doesn't contains the given first name
				List<Person> list = new ArrayList<>();
				list.add(p);
				map.put(p.getFirstName(), list);
			}
		}

		// print the result
		print(map);
	}

	private static void print(Map<String, List<Person>> map) {
		for (Map.Entry<String, List<Person>> entry : map.entrySet()) {
			System.out.println("��� ����� " + entry.getKey() + " ������ ����� :");
			System.out.println(entry.getValue().size());
		}
	}

	private static List<Person> getPersons() {

		List<Person> list = new ArrayList<>();
		for (int i = 0; i < 1000; i++) {
			list.add(generatePerson());
		}

		return list;
	}

	private static Person generatePerson() {
		Person p = new Person();
		p.setId(randomInt(100_000));
		p.setAge(randomInt(100));
		p.setFirstName("Name" + randomInt(10));
		p.setSecondName("Name" + randomInt(10));
		return p;
	}	

	private static int randomInt(int n) {
		return (int) (Math.random() * n);
	}

}
